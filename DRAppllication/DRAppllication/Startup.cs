﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DRAppllication.Startup))]
namespace DRAppllication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
